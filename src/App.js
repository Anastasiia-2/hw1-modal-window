import React, { useState } from "react";
import styles from "./App.module.css";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

function App() {
  const [isShowFirstModal, setIsShowFirstModal] = useState(false);
  const [isShowSecondModal, setIsShowSecondModal] = useState(false);

  const openFirstModal = () => {
    setIsShowFirstModal(prev => !prev);
  };

  const openSecondModal = () => setIsShowSecondModal(prev => !prev);

  return (
    <div className={styles.container}>
      <Button
        text="Open first modal"
        backgroundColor="red"
        handleBtnClick={openFirstModal}
      />
      <Button
        text="Open second modal"
        backgroundColor="orangered"
        handleBtnClick={openSecondModal}
      />
      {isShowFirstModal && (
        <Modal
          header="do you want to delete this file?"
          closeButton="true"
          handleBtnClick={openFirstModal}
          text="Are you sure you want to delete this file?"
          actions={
            <>
              <Button
                text="YES"
                backgroundColor="#d53434aa"
                handleBtnClick={openFirstModal}
              />
              <Button
                text="NO"
                backgroundColor="#d53434aa"
                handleBtnClick={openFirstModal}
              />
            </>
          }
        />
      )}

      {isShowSecondModal && (
        <Modal
          header="Second modal"
          closeButton={false}
          text="Do you want to delete this file?"
          handleBtnClick={openSecondModal}
          actions={
            <>
              <Button
                text="YES"
                backgroundColor="#d53434aa"
                handleBtnClick={openSecondModal}
              />
              <Button
                text="NO"
                backgroundColor="#d53434aa"
                handleBtnClick={openSecondModal}
              />
            </>
          }
        />
      )}
    </div>
  );
}

export default App;
