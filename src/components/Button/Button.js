import PropTypes from "prop-types";
import styles from "./Button.module.scss";

function Button({ text, handleBtnClick, backgroundColor, classBtn = null }) {
  return (
    <>
      <button
        className={styles.button}
        style={{ backgroundColor: backgroundColor || "#d53434aa" }}
        onClick={handleBtnClick}
        type="button"
      >
        {text}
      </button>
    </>
  );
}

Button.propTypes = {
  text: PropTypes.string,
  handleBtnClick: PropTypes.func,
  backgroundColor: PropTypes.string,
  classBtn: PropTypes.string,
};

export default Button;
